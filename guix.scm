(use-modules ((gnu packages haskell-xyz) #:select (ghc-attoparsec))
             ((guix build-system haskell) #:select (haskell-build-system))
             ((guix git-download) #:select (git-predicate))
             ((guix gexp) #:select (local-file))
             ((guix licenses) #:select (bsd-3))
             ((guix packages) #:select (origin package)))

(let
  ((%source-dir (dirname (current-filename))))
  (package
    (name "ghc-xmlfilter")
    (version "0.3.0.1")
    (source
      (local-file %source-dir
          #:recursive? #t
          #:select? (git-predicate %source-dir)))
    (build-system haskell-build-system)
    (inputs (list ghc-attoparsec))
    (home-page "https://gitlab.huma-num.fr/alicebrenon/XMLFilter")
    (synopsis "SAX-based library to handle XML-like documents")
    (description
      "Reads the files as streams of markup events to allow altering the file
      content on the fly and fixing broken files which aren't proper XML")
    (license bsd-3)))
