{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
module XML.Filter (
      Occurrence(..)
    , Position(..)
    , Node(..)
    , Temporary(..)
    , Result(..)
    , Strategy(..)
    , apply
    , escape
    , log
    , printNode
    , updateStream
  ) where

import Control.Applicative ((<|>))
import Control.Monad.Reader (ReaderT, ask, runReaderT)
import Control.Monad.State (StateT, evalStateT, get, modify)
import Control.Monad.Trans (lift)
import qualified Data.Attoparsec.Text as Atto (
    Parser, char, choice, notChar, parseOnly, string, takeWhile1
  )
import Data.List (inits)
import Data.Text (Text)
import qualified Data.Text as Text (concat, concatMap, pack, snoc, uncons)
import Prelude hiding (log, repeat, takeWhile)

data Position = Position {
      line :: Int
    , offset :: Int
  } deriving Show

data Node =
    Open {
        name :: Text
      , content :: Text
    }
  | Comment {
      content :: Text
    }
  | Autoclosing {
        name :: Text
      , content :: Text
    }
  | Close {
      name :: Text
    }
  | Declaration {
      content :: Text
    }
  | Special {
      content :: Text
    }
  | Text {
      content :: Text
    } deriving Show

printNode :: Node -> Text
printNode (Declaration content) = Text.concat ["<?", content, "?>"]
printNode (Special content) = Text.concat ["<!", content, ">"]
printNode (Comment content) = Text.concat ["<!--", content, "-->"]
printNode (Close name) = Text.concat ["</", name, ">"]
printNode (Open {name, content}) = Text.concat ["<", Text.concat [name, content], ">"]
printNode (Autoclosing {name, content}) = Text.concat ["<", Text.concat [name, content], "/>"]
printNode (Text {content}) = content

data Occurrence  = Occurrence {
      at :: Position
    , node :: Node
  } deriving Show

data Temporary a = Temporary {
      position :: Position
    , memory :: a
    , outputStack :: [Node]
    , logLines :: [Text]
  } deriving Show

instance Functor Temporary where
  fmap f state = state { memory = f $ memory state }

data Result a = Result {
      dump :: a
    , output :: Text
    , logs :: [Text]
  } deriving Show

data Strategy a = Strategy (Occurrence -> Temporary a -> Temporary a)

type Parser a b = ReaderT (Strategy a) (StateT (Temporary a) Atto.Parser) b

lift2 :: Atto.Parser b -> Parser a b
lift2 = lift . lift

log :: Text -> Temporary a -> Temporary a
log line state = state { logLines = line : logLines state }

updateStream :: ([Node] -> [Node]) -> Temporary a -> Temporary a
updateStream f state = state { outputStack = f $ outputStack state }

updatePosition :: (Position -> Position) -> Parser a ()
updatePosition f = modify $ \state -> state { position = f $ position state }

forth :: Int -> Position -> Position
forth delta position = position { offset = offset position + delta }

newLine :: Int -> Position -> Position
newLine delta (Position {line}) = Position { line = line + delta, offset = 0 }

scan :: Parser a (Temporary a)
scan = many handleOccurrence *> get

handleOccurrence :: Parser a ()
handleOccurrence = do
  at <- position <$> get
  node <- getNode
  Strategy reactTo <- ask
  modify . reactTo $ Occurrence {at, node}

getNode :: Parser a Node
getNode =
    contentNode
  <|> commentNode
  <|> closeNode
  <|> declarationNode
  <|> specialNode
  <|> textNode

char :: Char -> Parser a Char
char c = do
  (lift2 $ Atto.char c) <* updatePosition (if c == '\n' then newLine 1 else forth 1)

string :: Text -> Parser a Text
string s = (lift2 $ Atto.string s) >>= positionOffset

takeWhile :: (Char -> Bool) -> Parser a Text
takeWhile predicate = (lift2 $ Atto.takeWhile1 predicate) >>= positionOffset

upTo :: [Char] -> Parser a Text
upTo "" = return ""
upTo end =
  (repeat (lift2 choices) >>= positionOffset) <* string (Text.pack end)
  where
    generateChoice ("", c) = Atto.takeWhile1 (/= c)
    generateChoice (prefix, c) = Text.snoc <$> Atto.string prefix <*> Atto.notChar c
    choices = Atto.choice (generateChoice <$> zip (Text.pack <$> inits end) end)

positionOffset :: Text -> Parser a Text
positionOffset text =
  let (dLines, dOffset) = dPosition text in
  updatePosition (forth dOffset . newLine dLines) >> return text
  where
    dPosition someText =
      case Text.uncons someText of
        Nothing -> (0, 0)
        Just (c, rest) ->
          let (dLines, dOffset) = dPosition rest in (
              dLines + (if c == '\n' then 1 else 0)
            , dOffset + if dLines == 0 && c /= '\n' then 1 else 0
          )

many :: Parser a b -> Parser a [b]
many parser = ((:) <$> parser <*> many parser) <|> pure []

repeat :: Parser a Text -> Parser a Text
repeat = fmap Text.concat . many

contentNode :: Parser a Node
contentNode = do
  name <- char '<' *> takeWhile (not . (`elem` ['>', '/', ' ', '!', '?']))
  open name <|> autoclosing name
  where
    open name = Open name <$> repeat (regularChars <|> notAutoclosing) <* char '>'
    regularChars = takeWhile (not . (`elem` ['/', '>']))
    notAutoclosing = Text.snoc <$> string "/" <*> (lift2 $ Atto.notChar '>')
    autoclosing name = Autoclosing name <$> upTo "/>"

commentNode :: Parser a Node
commentNode = Comment <$> (string "<!--" *> upTo "-->")

closeNode :: Parser a Node
closeNode = Close <$> (string "</" *> takeWhile (/= '>') <* char '>')

declarationNode :: Parser a Node
declarationNode = Declaration <$> (string "<?" *> upTo "?>")

specialNode :: Parser a Node
specialNode = Special <$> (string "<!" *> upTo ">")

textNode :: Parser a Node
textNode = Text <$> takeWhile (/= '<')

initState :: a -> Temporary a
initState initValue = Temporary {
      position = Position { line = 1, offset = 1 }
    , memory = initValue
    , outputStack = []
    , logLines = []
  }

escape :: Text -> Text
escape = Text.concatMap escapeChar
  where
    escapeChar '<' = "&lt;"
    escapeChar '>' = "&gt;"
    escapeChar c = Text.pack [c]

apply :: Strategy a -> a -> Text -> Result a
apply strategy initValue input =
  let attoParser = evalStateT (runReaderT scan strategy) (initState initValue) in
  case Atto.parseOnly attoParser input of
    Left errorMessage -> Result {
          dump = initValue
        , output = ""
        , logs = [Text.pack errorMessage]
      }
    Right finalState -> Result {
          dump = memory finalState
        , output = Text.concat . fmap printNode . reverse $ outputStack finalState
        , logs = reverse $ logLines finalState
      }
