# XMLFilter

XMLFilter is a Haskell library designed to clean broken XML-TEI files.

Instead of trying to parse XML into trees properly, because it's made to handle
broken files, XMLFilter only defines the markup types that can be encountered
(including raw text) and lets you define a *strategy* to handle the markup as
they get read, allowing you to correct the flow on the fly.

## How to use it

### With [`guix`](https://guix.gnu.org/)

The fastest way to try this library from a clone of this repos is to spawn a
`guix` shell where it is installed. Don't forget to include `ghc` (even if you
have it installed in your user or system profile) or `ghci` won't find
XMLFilter.

```sh
guix shell ghc -f guix.scm
```

If you're satisfied, you can use install it to your user profile with `guix
install` (same `-f` option). Note that this lets you install a development
version from the local state of your clone of the repository (including possible
changes you've made).

For a more reproducible setup, please use the stable version distributed with
the [channel](https://gitlab.liris.cnrs.fr/geode/guix-packages) for project
[GEODE](https://geode-project.github.io/). Once you have told your `guix`
install about this channel, this package is available as `ghc-xmlfilter`:

```sh
guix search ghc-xmlfilter
name: ghc-xmlfilter
version: 0.3.1.0
outputs: out
…
```

It can then be used in any guix file, for instance in a manifest:

```guile
(packages->manifest (list ghc ghc-xmlfilter))
```

### With [cabal](https://www.haskell.org/cabal/)

Where `guix` isn't an option, you can still use it as a regular `cabal` package
and install it with:


It can be built and installed with [`cabal`](https://www.haskell.org/cabal/).

```
cabal new-update
cabal new-build
cabal new-install
```
