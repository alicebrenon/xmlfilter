# Revision history for XMLFilter

## 0.3.1.1  -- 2023-05-05

* Upgrade base version to support 4.16

## 0.3.1.0  -- 2022-09-05

* First version, packaged with cabal and guix
